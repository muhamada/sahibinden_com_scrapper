# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class SahibindenItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    link = scrapy.Field()
    name = scrapy.Field()
    land_line_0 = scrapy.Field()
    land_line_1 = scrapy.Field()
    land_line_2 = scrapy.Field()
    mobile_0 = scrapy.Field()
    mobile_1 = scrapy.Field()
    mobile_2 = scrapy.Field()
    pass
