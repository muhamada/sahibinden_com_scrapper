# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.linkextractors import LinkExtractor

from scrapy.contrib.spiders import CrawlSpider, Rule

from sahibinden.items import SahibindenItem
from scrapy import log

class Son1AySpider(CrawlSpider):
    name = 'son_1_ay'
    allowed_domains = ['sahibinden.com']
    start_urls = ['http://www.sahibinden.com/son-1-ay']

    rules = (
        Rule(LinkExtractor(allow=(''), restrict_xpaths=('//a[contains(.,"Sonraki")]')), callback='parse_listings_page', follow=True),
        Rule(LinkExtractor(allow=(''), restrict_xpaths=('//a[@class="classifiedTitle"]')), callback='parse_item_page', follow=True),
    )

    def parse_start_url(self, response):
        return self.parse_listings_page(response)

    def parse_listings_page(self, response):
        log.msg('Scrapping Listings Page: ' + response.url)

    def parse_item_page(self, response):
        log.msg('Scrapping Item Page: ' + response.url)
        item = SahibindenItem()
        item['link'] = response.url
        item['name'] = response.xpath('//div[contains(@class, "classifiedUserContent")]/h5/text()').extract()
        log.msg("Scrapping Item's land line numbers:")
        land_line = response.xpath(u'//div[contains(@class, "classifiedUserContent")]/ul/li/strong[contains(.,"İş")]/../span/text()')
        log.msg(str(len(land_line)) + " land line numbers ")

        if len(land_line) > 0:
            item['land_line_0'] = land_line[0].extract()
            if len(land_line) >= 2:
                item['land_line_1'] = land_line[1].extract()
                if len(land_line) >= 3:
                    item['land_line_2'] = land_line[2].extract()

        log.msg("Scrapping Item's mobile numbers:")
        mobile = response.xpath(u'//div[contains(@class, "classifiedUserContent")]/ul/li/strong[contains(.,"Cep")]/../span/text()')
        log.msg(str(len(mobile)) + " mobile numbers")
        if len(mobile) > 0:
            item['mobile_0'] = mobile[0].extract()
            if len(mobile) >= 2:
                item['mobile_1'] = mobile[1].extract()
                if len(mobile) >= 3:
                    item['mobile_2'] = mobile[2].extract()
        return item
