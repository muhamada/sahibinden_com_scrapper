# Web-scrapper for "sahibinden.com" by Mohamed A. Hasan
This web scrapper the contents of the directory by doing the following:
1. Going to the page `http://www.sahibinden.com/son-1-ay` and go ito each listing page.
2. In the item page it gather the data (Link, Name, Land Lines and Mobile numbers)
3. Look for the `Next` button in the listings page to go through the next page and process more items.
4. When is the `Next` button is no longer available, the spider stops.

## Installing the Spider Locally
The following instructions is tested on Ubuntu 14.04
### Prerequisites
#### Python 2.7
Python 2.7 is usually installed by default, but you can install it by:
```
$ sudo apt-get install python2.7
```

#### lxl Dependencies
```
$ sudo apt-get install libxml2 libxslt1.1
```
#### PIP
```
$ sudo apt-get install python-pip
```
[Installing pip/setuptools/wheel with Linux Package Managers](https://packaging.python.org/en/latest/install_requirements_linux/#debian-ubuntu)


#### VirtualEnv and VirtualEnvWrapper
```
$ sudo pip install virtualenvwrapper
```
[Basic Installation](http://virtualenvwrapper.readthedocs.org/en/latest/install.html#basic-installation)


### Create a VirtualEnv and Installing Required Python Packages
1. Create a VirtualEnv
```
$ mkvirtualen sahibinden_com_scrapper
```
2. Switch to the `sahibinden_com_scrapper` virtualenv
```
$ workon sahibinden_com_scrapper
```
3. Switch to the directory of the scrapy project
Assuming that the spider is at `~/sahibinden_com_scrapper`
```
(sahibinden_com_scrapper)$ cd ~/sahibinden_com_scrapper/
```
4. Install required packages
```
(sahibinden_com_scrapper)$ pip install -r requirements.txt
```

## Running the Spider
To start scraping data and write the data to `output.csv`
```
(sahibinden_com_scrapper)$ cd sahibinden
(sahibinden_com_scrapper)$ scrapy crawl son_1_ay -o output.csv
```
[Scrapy Command Line Tool](http://doc.scrapy.org/en/0.24/topics/commands.html#crawl)

## Deploying the Spider to Scrapping Hub
1. Create a Scrapy project from  [Scrapping Hub Dashboard](https://dash.scrapinghub.com)
2. From `Code & Deploys`, copy the configurations and past them `sahibinden/scrapy.cfg`
3. In the project's director run `scrapy deploy`

## Activating Crawlera to Enable IP Switching

At the end of `setting.py`, uncomment the block titled `Crawlera Configurations` and replace `<API Key>` with one you get from Scrapping Hub

(Using Crawlera with Scrapy)[http://doc.scrapinghub.com/crawlera.html#using-crawlera-with-scrapy]